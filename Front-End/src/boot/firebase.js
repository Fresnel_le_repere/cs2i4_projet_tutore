import firebase from 'firebase/app'

const firebaseConfig = {
    apiKey: 'AIzaSyDBkBjMbx2VnmPTusF9eM6I8WYV7ixsK_U',
    authDomain: 'camer-alert.firebaseapp.com',
    projectId: 'camer-alert',
    storageBucket: 'camer-alert.appspot.com',
    messagingSenderId: '47387748254',
    appId: '1:47387748254:web:cc8ac02d7e4f1ca892188f',
    measurementId: 'G-W975J24HNR'
}

const firebaseApp = firebase.initializeApp(firebaseConfig)
const db = firebase.firestore()
const firebaseAuth = firebaseApp.auth()
export { db, firebaseApp, firebaseAuth }