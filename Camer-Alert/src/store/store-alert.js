import { firebaseAuth, db } from 'boot/firebase';
import { Notify, LocalStorage } from 'quasar';

const state = {
  Acounts: [],
};

const mutations = {
  setAccount(Acounts) {
    state.Acounts = Acounts;
  },
  register() {
    Notify.create({
      color: 'green-4',
      textColor: 'white',
      type: 'positive',
      message: 'Compte crée avec success',
      position: 'top',
    });
  },
  connected() {
    Notify.create({
      color: 'green-4',
      textColor: 'white',
      type: 'positive',
      message: 'Connexion avec success',
      position: 'top',
    });
  },
  editUser() {
    Notify.create({
      color: 'green-4',
      textColor: 'white',
      type: 'positive',
      message: 'Compte modifié avec success',
      position: 'top',
    });
  },
  logIn(user) {
    db.collection('Citizen').where('Account.email', '==', user.email)
      .get()
      .then((query) => {
        query.forEach((doc) => {
          LocalStorage.set('user', doc.data());
          console.log('query : ', LocalStorage.getItem('user'));
        });
        LocalStorage.set('user', query);
        console.log('user : ', query);
      })
      .catch((error) => {
        console.log('Error getting documents: ', error);
      });
  },
};

const actions = {

  async register({ dispatch, commit }, citizen) {
    await firebaseAuth.createUserWithEmailAndPassword(
      citizen.Account.email,
      citizen.Account.password,
    ).then(async () => {
      await db.collection('User').add(citizen)
        .then(() => {
          commit('register');
          dispatch('logOut');
        });
    }).catch((error) => {
      console.log('Error : ', error);
    });
  },

  async logOut() {
    await firebaseAuth.signOut().then(() => {
      this.$router.push('/');
      LocalStorage.clear();
    }).catch((error) => {
      console.log('Error : ', error);
    });
  },

  async logIn({ commit, dispatch }, user) {
    await firebaseAuth.signInWithEmailAndPassword(
      user.email, user.password,
    ).then(() => {
      dispatch('getUserByMail', user);
      commit('logIn', user);
    }).catch((error) => {
      console.log('Error : ', error);
    });
  },

  async getUserByMail({ commit }, user) {
    console.log('user : ', user.email);
    await db.collection('User').where('Account.email', '==', user.email)
      .get()
      .then((query) => {
        console.log('user : ', user.email);
        query.forEach((doc) => {
          LocalStorage.set('user', { id: doc.id, data: doc.data() });
          this.$router.push('/home');
          console.log('query : ', doc.id);
        });
      })
      .catch((error) => {
        console.log('Error getting documents: ', error);
      });
    commit('connected');
  },

  async editUser({ commit }, user) {
    const doc = db.collection('User').doc(user.id);
    await db.runTransaction((transaction) => transaction.get(doc).then((sfDoc) => {
      if (!sfDoc.exists) {
        // throw 'Document does not exist!';
      }
      transaction.update(doc, user);
    }))
      .then((newUser) => {
        console.log('new user', newUser);
        commit('editUser');
      }).catch((err) => {
        // This will be an "population is too big" error.
        console.error(err);
      });
    console.log(doc);
    console.log(user.id);
    console.log(user);
  },
};

const getters = {

};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
