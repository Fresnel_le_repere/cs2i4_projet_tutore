import { db } from 'boot/firebase';
import firebase from 'firebase/app';
import { Notify, LocalStorage } from 'quasar';
// import { mapActions } from 'vuex';

const state = {
  Book: {},
  doctor: {},
};

const mutations = {
  async setBook() {
    const user = LocalStorage.getItem('user');
    console.log('user : ', user);
    const healthBookRef = await db.collection('HealthBook').doc(user.data.HealthBookId);
    await db.runTransaction((transaction) => transaction.get(healthBookRef).then((sfDoc) => {
      if (sfDoc.exists) {
        transaction.get(healthBookRef).then((data) => {
          state.Book = data.data();
          console.log('new healthBook', data.data());
        });
      }
    }))
      .then(() => {
        // console.log('new healthBook', healthBook);
      }).catch((err) => {
        // This will be an "population is too big" error.
        console.error(err);
      });
  },
  async setDoctor() {
    const user = LocalStorage.getItem('user');
    console.log('user : ', user);
    const doctorRef = await db.collection('Doctor').doc(user.data.DoctorId);
    await db.runTransaction((transaction) => transaction.get(doctorRef).then((sfDoc) => {
      if (sfDoc.exists) {
        transaction.get(doctorRef).then((data) => {
          state.doctor = data.data();
          console.log('new doctor', data.data());
        });
      }
    }))
      .then(() => {
        // console.log('new healthBook', healthBook);
      }).catch((err) => {
        // This will be an "population is too big" error.
        console.error(err);
      });
  },

  healthNotif() {
    Notify.create({
      color: 'green-4',
      textColor: 'white',
      type: 'positive',
      message: 'Action prise en compte',
      position: 'top',
    });
  },
  editUser() {
    Notify.create({
      color: 'green-4',
      textColor: 'white',
      type: 'positive',
      message: 'Compte modifié avec success',
      position: 'top',
    });
  },
};

const actions = {
  async getUserByMail({ commit }, email) {
    console.log('user : ', email);
    await db.collection('User').where('Account.email', '==', email)
      .get()
      .then((query) => {
        query.forEach((docUser) => {
          LocalStorage.set('user', { id: docUser.id, data: docUser.data() });
        });
      })
      .catch((error) => {
        console.log('Error getting documents: ', error);
      });
    commit('register');
  },
  async getHealthBookByUser({ commit }) {
    commit('setBook');
  },

  async getDoctorByUser({ commit }) {
    commit('setDoctor');
  },

  async Edit({ commit, dispatch }, healthBook) {
    const user = LocalStorage.getItem('user');
    const userRef = db.collection('User').doc(user.id);
    await db.runTransaction((transaction) => transaction.get(userRef).then((sfDoc) => {
      if (sfDoc.exists) {
        transaction.update(userRef, { HealthBookId: healthBook.id });
      }
    }))
      .then(async () => {
        dispatch('getUserByMail', user.data.Account.email);
        const healthBookRef = await db.collection('HealthBook').doc(healthBook.id);
        await db.runTransaction((transaction) => transaction.get(healthBookRef).then(() => {
          transaction.set(healthBookRef, healthBook);
        }))
          .then(() => {
            console.log('new healthBook', healthBook);
            commit('healthNotif');
          }).catch((err) => {
            console.error(err);
          });
      }).catch((err) => {
        console.error(err);
      });
  },

  async AddHealthInfo({ commit, dispatch }, healthInfo) {
    const user = LocalStorage.getItem('user');
    const healthInfoRef = await db.collection('HealthBook').doc(user.data.HealthBookId);
    await healthInfoRef.update({
      HealthInfos: firebase.firestore.FieldValue.arrayUnion(healthInfo),
    });
    dispatch('getUserByMail', user.data.Account.email);
    commit('healthNotif');
  },

  async AddDoctor({ commit, dispatch }, doctor) {
    const user = LocalStorage.getItem('user');
    const userRef = db.collection('User').doc(user.id);
    await db.runTransaction((transaction) => transaction.get(userRef).then((sfDoc) => {
      if (sfDoc.exists) {
        transaction.update(userRef, { DoctorId: doctor.id });
      }
    }))
      .then(async () => {
        dispatch('getUserByMail', user.data.Account.email);
        const doctorRef = await db.collection('Doctor').doc(doctor.id);
        await db.runTransaction((transaction) => transaction.get(doctorRef).then(() => {
          transaction.set(doctorRef, doctor);
        }))
          .then(() => {
            console.log('new doctor', doctor);
            commit('healthNotif');
          }).catch((err) => {
            console.error(err);
          });
      }).catch((err) => {
        console.error(err);
      });
  },
};

const getters = {
  getBook: () => state.Book,
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
