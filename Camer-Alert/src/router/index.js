import Vue from 'vue';
import VueRouter from 'vue-router';
import { firebaseAuth } from 'boot/firebase';

import routes from './routes';

Vue.use(VueRouter);

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default function (/* { store, ssrContext } */) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    // Leave these as they are and change in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE,
  });

  Router.beforeEach((to, from, next) => {
    const requiresAuth = to.matched.some((item) => item.meta.requiresAuth);
    const isAuthenticated = firebaseAuth.currentUser;
    if (requiresAuth && !isAuthenticated) {
      next('/');
    } else {
      // const val = to.matched.some((item) => {
      //   console.log('voir : ', item.instances.default);
      //   // console.log('voir : ', item.instances.default.essentialLinks);
      //   return true;
      // });
      // console.log('voir : ', val);
      next();
    }
  });
  return Router;
}
