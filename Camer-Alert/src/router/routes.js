const routes = [
  {
    path: '/',
    component: () => import('pages/Connexion.vue'),
  },
  {
    path: '/home',
    meta: { requiresAuth: true },
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/', component: () => import('pages/Index.vue') },
      { path: '/HealthBook', component: () => import('pages/HealthBook.vue') },
      { path: '/Warners', component: () => import('pages/Warners.vue') },
      { path: '/Saviors', component: () => import('pages/Saviors.vue') },
      { path: '/AlertReceived', component: () => import('pages/AlertReceived.vue') },
      { path: '/AlertIssued', component: () => import('pages/AlertIssued.vue') },
      { path: '/Profile', component: () => import('pages/Profile.vue') },
      { path: '/Tracking', component: () => import('pages/Tracking.vue') },
      { path: '/Alert', component: () => import('pages/Alert.vue') },
      { path: '/Report', component: () => import('pages/Report.vue') },
    ],
  },
  {
    path: '/inscription',
    component: () => import('pages/Inscription.vue'),
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue'),
  },
];

export default routes;
